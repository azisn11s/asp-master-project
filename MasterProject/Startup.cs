﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MasterProject.Startup))]
namespace MasterProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
